<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Municipalities\Models\Municipality;
use App\Modules\Cities\Models\City;
use App\Modules\MunicipalityPropertyValues\Models\MunicipalityPropertyValue;
use Illuminate\Support\Facades\DB;

class Nc {

    public $zip;
    public $municipality_id;
    public $city_id;
    public $id;

    public function __construct($data) {
        $this->zip = $data['zip'];
        $this->municipality_id = $data['municipality_id'];
        $this->city_id = $data['city_id'];
        $this->id = $data['id'];
    }

}

class WeatherIngest extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingest:weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ingests weather data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::statement('TRUNCATE TABLE `weather`');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $this->info('Weather ingestion started..........');
        $municipalities = MunicipalityPropertyValue::with('municipality.city')->where('property_id', '=', '7')->get()->toArray();

//        print_r($municipalities);
        for ($i = 0; $i < count($municipalities); $i++) {
//            if ($zip = $municipalities[$i]['value']) {
            $municipality = $municipalities[$i];
//                $municipality_id = $municipalities[$i]['municipality_id'];
            $this->call_me($municipality);
//            }
        }

        $this->info('Weather ingestion completed..........');

//        $this->revoke();
    }

    private function call_me($municipality) {

        $zip = $municipality['value'];
        $municipality_id = $municipality['municipality_id'];
        $city = $municipality['municipality']['city'];

        $city_id = 0;
        if ($city) {
            $city_id = $city['id'];
        }

        $this->call_api_async($zip, function($response) use(&$zip, &$municipality_id, &$city_id) {

            $weather = $response->getBody();
            $weather_details = array(
                "municipality_id" => $municipality_id,
                "zip" => $zip,
                "city_id" => $city_id,
                "weather" => $weather,
            );

            $weather_id = DB::table('weather')->insertGetId($weather_details);
            print_r(" \n " . "Successfully Inserted : " . $weather_id . " \n ");
        }, function ($error) use(&$zip, &$municipality_id, &$city_id) {

            print_r(" \n " . "Retrying to insert with city id: " . $city_id . " \n ");
            $weather_details = array(
                "municipality_id" => $municipality_id,
                "zip" => $zip,
                "city_id" => $city_id,
                "error" => $error
            );
            $weather_id = DB::table('not_inserted_weather')->insertGetId($weather_details);

            $ob = new Nc(array(
                'zip' => $zip,
                'municipality_id' => $municipality_id,
                'city_id' => $city_id,
                'id' => $weather_id
            ));

            $this->call_me_city($ob);
        });
    }

    private function call_me_city($ob) {

        $zip = $ob->zip;
        $municipality_id = $ob->municipality_id;
        $city_id = $ob->city_id;
        $id = $ob->id;

        $this->call_api_async_cityid($city_id, function($response) use(&$zip, &$municipality_id, &$id, &$city_id) {

            $weather = $response->getBody();
            $weather_details = array(
                "municipality_id" => $municipality_id,
                "zip" => $zip,
                "city_id" => $city_id,
                "weather" => $weather,
            );

            $weather_id = DB::table('weather')->insertGetId($weather_details);
            print_r(" \n " . "Successfully Inserted : " . $weather_id . " \n ");

            DB::table('not_inserted_weather')->where('id', '=', $id)->delete();
        }, function ($error) use(&$zip, &$municipality_id, &$id, &$city_id) {

            print_r(" \n " . "Not Inserted : " . $municipality_id . ", " . $city_id . " \n ");
//            $weather_details = array(
//                "municipality_id" => $municipality_id,
//                "zip" => $zip,
//                "error" => $error
//            );
//            $weather_id = DB::table('not_inserted_weather')->insertGetId($weather_details);
        });
    }

    private function revoke() {

        $list = DB::select(DB::raw('SELECT m.name, cities.id as city_id, nw.zip, nw.municipality_id, nw.id '
                                . 'from not_inserted_weather as nw '
                                . 'INNER JOIN municipalities as m ON m.id = nw.municipality_id '
                                . 'INNER JOIN cities ON cities.name = m.name'));

        if (count($list) > 0) {
            $this->info('Revoking uninserted data ingesion started:...............');
            foreach ($list as $item) {
                $this->call_me_city($item);
            }
            $this->info('Revoking uninserted data ingesion ended:.................');
        }
    }

    private function call_api($zip = '', $city_name = '', $country = 'IT') {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://api.openweathermap.org/data/2.5/weather?q=' . $city_name . '&appid=bbefbd979254a1ce4ea89756e827bcf4');
        return $res->getBody();
    }

    private function call_api_async($zip, $success, $error, $country = 'IT') {
        $client = new \GuzzleHttp\Client();
        // Send an asynchronous request.
        $request = new \GuzzleHttp\Psr7\Request('GET', 'http://api.openweathermap.org/data/2.5/weather?zip=' . $zip . ',' . $country . '&appid=bbefbd979254a1ce4ea89756e827bcf4');
        $promise = $client->sendAsync($request)->then($success, $error);
        $promise->wait();
    }

    private function call_api_async_cityid($city_id, $success, $error) {
        $client = new \GuzzleHttp\Client();
        // Send an asynchronous request.
        $request = new \GuzzleHttp\Psr7\Request('GET', 'http://api.openweathermap.org/data/2.5/weather?id=' . $city_id . '&appid=bbefbd979254a1ce4ea89756e827bcf4');
        $promise = $client->sendAsync($request)->then($success, $error);
        $promise->wait();
    }

}
