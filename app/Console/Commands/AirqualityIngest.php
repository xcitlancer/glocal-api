<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Municipalities\Models\Municipality;
use App\Modules\Airqualities\Models\Airquality;
use Exception;
use Illuminate\Support\Facades\DB;



class AirqualityIngest extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingest:airquality';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ingests air quality';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::statement('TRUNCATE TABLE `airqualities`');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $this->info('aiquality ingestion started..........');

        $municipalities = Municipality::with('city')->get();

        foreach ($municipalities as $key_municipality => $value_municipality) {
            $municipality_id = $value_municipality->id;
            if(!$value_municipality->city){

                echo 'Not Inserted '.$value_municipality->id;
                $error_arr = array("id"=>$municipality_id,"error"=>'no match in city table');

                try {
                    DB::table('not_inserted_airqualities')->insertGetId($error_arr);

                }
                catch (Exception $e) {
                    echo 'not inserted insertion failed';
                }
                continue;
            }
            $city_lat = $value_municipality->city->lat;
            $city_lon = $value_municipality->city->lon;
            $this->info($city_lat.', '.$city_lon);

            $this->call_api_async($city_lat, $city_lon, function($response) use ($municipality_id){
            try {
                $airquality = new Airquality;
                $airquality->municipality_id = $municipality_id;
                $airquality->airquality_details = $response->getBody();
                $airquality->save();
            } catch (Exception $e) {
                echo 'Not Inserted '.$municipality_id;
                $error_arr = array("id"=>$municipality_id,"error"=>'airquality insertion failed');

                try {
                    DB::table('not_inserted_airqualities')->insertGetId($error_arr);

                }
                catch (Exception $e) {
                    echo 'not inserted insertion failed';
                }
            }



            },function($error) use($municipality_id){
                echo 'Not Inserted '.$municipality_id;
                $error_arr = array("id"=>$municipality_id,"error"=>$error);

                try {
                    DB::table('not_inserted_airqualities')->insertGetId($error_arr);

                }
                catch (Exception $e) {
                    echo 'not inserted insertion failed';
                }

            });





        }

        $this->info('aiquality ingestion completed ..........');


    }
    private function call_api_async($city_lat, $city_lon, $success, $error) {
        $client = new \GuzzleHttp\Client();
        // Send an asynchronous request.
        $request = new \GuzzleHttp\Psr7\Request('GET', 'https://api.waqi.info/feed/geo:'.$city_lat.';'.$city_lon.'/?token=830d7d4b8045d20c5f57f410752ffdfdb93b1937');
        $promise = $client->sendAsync($request)->then($success, $error);
        $promise->wait();
    }


}
