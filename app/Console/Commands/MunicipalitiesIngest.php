<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;

class MunicipalitiesIngest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingest:municipality';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ingests regions, provinces, zones, municipalities,cities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Monthly municipality ingestion started..........');
        $path = base_path() . "\json\comuni.json";
        $json = json_decode(file_get_contents($path), true);

        $not_created = array();
        DB::beginTransaction();
        try {
            $this->truncate();
            foreach ($json as $key => $values) {

                $zone = array(
                    "name" => $values['zona']['nome'],
                    "code" => $values['zona']['codice'],
                );

                $zone_ob = DB::table('zones')->select('id')
                                ->where("name", $zone['name'])
                                ->where("code", $zone['code'])
                                ->get()->first();

                if ($zone_ob) {
                    $zone_id = $zone_ob->id;
                } else {
                    $zone_id = DB::table('zones')->insertGetId($zone);
                }

                $regione = array(
                    "name" => $values['regione']['nome'],
                    "code" => $values['regione']['codice'],
                );

                $regione_ob = DB::table('regions')->select('id')
                                ->where("name", $regione['name'])
                                ->where("code", $regione['code'])
                                ->get()->first();

                if ($regione_ob) {
                    $regione_id = $regione_ob->id;
                } else {
                    $regione_id = DB::table('regions')->insertGetId($regione);
                }

                $provincia = array(
                    "name" => $values['provincia']['nome'],
                    "code" => $values['provincia']['codice'],
                    "region_id" => $regione_id,
                );

                $provincia_ob = DB::table('provinces')->select('id')
                                ->where("name", $provincia['name'])
                                ->where("code", $provincia['code'])
                                ->get()->first();

                if ($provincia_ob) {
                    $provincia_id = $provincia_ob->id;
                } else {
                    $provincia_id = DB::table('provinces')->insertGetId($provincia);
                }

                $communi_id = null;

                $communi = array(
                    'name' => $values['nome'],
                    'code' => $values['codice'],
                    'zone_id' => $zone_id,
                    'region_id' => $regione_id,
                    'province_id' => $provincia_id,
                );

                $communi_ob = DB::table('municipalities')->select('id')
                                ->where("name", $provincia['name'])
                                ->where("code", $provincia['code'])
                                ->where("zone_id", $zone_id)
                                ->where("region_id", $regione_id)
                                ->where("province_id", $provincia_id)
                                ->get()->first();

                if ($communi_ob) {
                    $communi_id = $communi_ob->id;
                }

                $properties = [
                    'sigla' => $values['sigla'],
                    'codice_catastale' => $values['codiceCatastale'],
                    'caps' => implode(',', $values['cap']),
                    'population' => $values['popolazione'],
                ];

                if ($zone_id && $regione_id && $provincia_id && !$communi_id) {
                    $communi_id = DB::table('municipalities')->insertGetId($communi);
                    $municipility_properties = array();
                    foreach ($properties as $key => $value) {
                        $property_ob = DB::table('properties')->select('id')
                                        ->where("name", $key)
                                        ->get()->first();
                        if ($property_ob) {
                            $property_id = $property_ob->id;
                        } else {
                            $property_id = DB::table('properties')->insertGetId([
                                'name' => $key
                            ]);
                        }
                        array_push($municipility_properties, [
                            'property_id' => $property_id,
                            'municipality_id' => $communi_id,
                            'value' => $value
                        ]);
                    }
                    DB::table('municipality_property_values')->insert($municipility_properties);
                }

                if ($communi_id) {

                } else {
                    echo "NOT CREATED" . $values['nome'];
                    array_push($not_created, $values['nome']);
                }
            }
        } catch (Exception $e) {
            print_r($e);
            DB::rollBack();
        }

        DB::commit();
        $this->info('Monthly municipality ingestion completed..........');
		
    }

    private function truncate() {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::statement('TRUNCATE TABLE `municipality_property_values`');
        DB::statement('TRUNCATE TABLE `municipalities`');
        DB::statement('TRUNCATE TABLE `provinces`');
        DB::statement('TRUNCATE TABLE `regions`');
        DB::statement('TRUNCATE TABLE `zones`');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        return "Success";
    }

    public function butify($d) {
        echo "<pre>";
        print_r($d);
        echo "</pre>";
    }
}
