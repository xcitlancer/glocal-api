<?php

namespace App\Modules\Zones\Models;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    public function municipalities()
    {
        return $this->hasMany('App\Modules\Municipalities\Models\Municipality');
    }
}
