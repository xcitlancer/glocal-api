<?php

namespace App\Modules\TopMunicipalities\Models;

use Illuminate\Database\Eloquent\Model;

class TopMunicipality extends Model 
{
    public function municipality()
    {
        return $this->belongsTo('App\Modules\Municipalities\Models\Municipality');
    }
    
}
