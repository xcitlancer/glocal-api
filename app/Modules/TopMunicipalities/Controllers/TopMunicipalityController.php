<?php

namespace App\Modules\TopMunicipalities\Controllers;
use App\Modules\Common\Common;
use App\Modules\TopMunicipalities\Models\TopMunicipality;
use App\Modules\Regions\Models\Region;
use App\Modules\Provinces\Models\Province;
use App\Modules\Municipalities\Models\Municipality;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class TopMunicipalityController extends Controller
{
	// get top municipalities list with pagination , serach
    public function getTopMunicipalities(Request $request)
    {
		
		$all=$request->all();
		
		if(empty($all['page_no']) && empty($all['_page']))
		{
			$page_no=0;
		}
		else
		{
			$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
		}
		if(empty($all['no_of_rows'])&& empty($all['_limit']))
		{
			$no_of_rows=0;
		}else{
			$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
		}
		
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$offset = ($page_no - 1) * $no_of_rows;
		
		if(empty($page_no))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
		}
		if(empty($no_of_rows))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
		}
		
		$totalrows=TopMunicipality::select('id');
		$municipalities = TopMunicipality::with(['municipality']);
		
		if(!empty($all['municipality_like']))
		{
			$municipality_name=$all['municipality_like'];
			$municipalities=$municipalities->whereHas('municipality',function($q) use ($municipality_name) {
					$q->where('name', 'LIKE', "$municipality_name%");
				});
			$totalrows=$totalrows->whereHas('municipality',function($q) use ($municipality_name) {
				$q->where('name', 'LIKE', "$municipality_name%");
			});
		}
		$municipalities=$municipalities->orderBy('id',$order_type)->skip($offset)->take($no_of_rows)->get();
		$totalrows=$totalrows->count();
				
        return response()->json(['status'=>1,'message'=>'success','data'=>$municipalities,'thumbnail_folder_path'=>'municipalityimages/thumbnail','medium_folder_path'=>'municipalityimages/medium','original_folder_path'=>'municipalityimages/original','totalrows'=>$totalrows]);
	}
	
	// add top municipality
	public function topMunicipalityAdd(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['municipality_id']))
		{
			$municipality_id=0;
			
		}
		else
		{
			$municipality_id=$all['municipality_id'];
		}
		if(empty($municipality_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input municipality_id','data'=>[]]);
		}
		else
		{
			//puplar status update
			$municipality = Municipality::find($municipality_id);
			$municipality->is_popular = 1;
			$municipality->save();

			$municipality = new TopMunicipality;
			$municipality->municipality_id = $municipality_id;
			$municipality->save();	
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}
	//delete top municipality
	public function topMunicipalityDelete(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['id']))
		{
			$id=0;
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input id','data'=>[]]);
			
		}
		else
		{
			$id=$all['id'];
		}
		$municipality_count = TopMunicipality::select('id')->where('id', $id)->count();
			
		if($municipality_count>0)
		{
			//puplar status update
			$top_municipality_data = TopMunicipality::select('municipality_id')->where('id', $id)->get();
			$municipality_id=$top_municipality_data[0]['municipality_id'];
			$municipality = Municipality::find($municipality_id);
			$municipality->is_popular = 0;
			$municipality->save();

			TopMunicipality::select('id')->where('id', $id)->delete();
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
		else
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
	}

	//get all top municipalities without pagination

	public function getAllTopMunicipalities(Request $request)
    {
		$all=$request->all();

		//search municipality with query parameter
		if(empty($all['query']))
		{
			$query_value='';
		}
		else
		{
			$query_value=$all['query'];
		}
		if(!empty($query_value)){

			if(empty($all['page_no']) && empty($all['_page']))
			{
				$page_no=1;
			}
			else
			{
				$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
			}
			if(empty($all['no_of_rows'])&& empty($all['_limit']))
			{
				$no_of_rows=12;
			}else{
				$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
			}
			
			if(empty($all['order_type']) && empty($all['_order']))
			{
				$order_type="asc";
			}
			else{
				$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
			}
			$offset = ($page_no - 1) * $no_of_rows;
			
			if(empty($page_no))
			{
				//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
				return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
			}
			if(empty($no_of_rows))
			{
				//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
				return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
			}

			$totalrows_region=Region::select('id')->where('name',$query_value)->count();
			if($totalrows_region==1){
				$regioninfo=Region::select('id','name')->where('name',$query_value)->get();
				$regionid=$regioninfo[0]['id'];
			}else{
				$regionid=0;
			}

			$totalrows_province=Province::select('id')->where('name',$query_value)->count();
			if($totalrows_province==1){
				$provinceinfo=Province::select('id','name')->where('name',$query_value)->get();
				$provinceid=$provinceinfo[0]['id'];
			}else{
				$provinceid=0;
			}
			$municipalities = Municipality::
				with(['region','province','weather','airquality','municipalityImages'])->Where('name', 'LIKE', "$query_value%");
			$totalrows=Municipality::select('id')->Where('name', 'LIKE', "$query_value%");

			if($regionid>0){
				$municipalities=$municipalities->orWhere('region_id', $regionid);
				$totalrows=$totalrows->orWhere('region_id', $regionid);
			}
			if($provinceid>0){
				$municipalities=$municipalities->orWhere('province_id', $provinceid);
				$totalrows=$totalrows->orWhere('province_id', $provinceid);
			}
			$municipalities=$municipalities->orderBy('name',$order_type)->skip($offset)->take($no_of_rows)->get();
		    $totalrows=$totalrows->count();
			return response()->json(['status'=>1,'message'=>'success','data'=>$municipalities,'thumbnail_folder_path'=>'municipalityimages/thumbnail','medium_folder_path'=>'municipalityimages/medium','original_folder_path'=>'municipalityimages/original','totalrows'=>$totalrows]);

		}

		$totalrows=TopMunicipality::select('id');
		$municipalities = TopMunicipality::with(['municipality','municipality.municipalityImages','municipality.region','municipality.province','municipality.weather','municipality.airquality'])->orderBy('id','asc')->get();
		$totalrows=$totalrows->count();
        return response()->json(['status'=>1,'message'=>'success','data'=>$municipalities,'thumbnail_folder_path'=>'municipalityimages/thumbnail','medium_folder_path'=>'municipalityimages/medium','original_folder_path'=>'municipalityimages/original','totalrows'=>$totalrows]);
	}
}
