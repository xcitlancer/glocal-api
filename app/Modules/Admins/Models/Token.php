<?php

namespace App\Modules\Admins\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    public function admin()
    {
        return $this->belongsTo('App\Modules\Admins\Models\Admin');
    }
}
