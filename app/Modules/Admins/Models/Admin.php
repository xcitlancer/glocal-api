<?php

namespace App\Modules\Admins\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public function tokens()
    {
        return $this->hasMany('App\Modules\Admins\Models\Token');
    }
}
