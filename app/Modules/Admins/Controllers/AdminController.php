<?php

namespace App\Modules\Admins\Controllers;
use App\Modules\Common\Common;
use App\Modules\Admins\Models\Admin;
use App\Modules\Admins\Models\Token;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function login(Request $request)
    {
        $all=$request->all();
        $admin=Admin::where('email',$all['email'])->where('password',md5($all['password']))->get();
        if(count($admin)>0){
            $status=1;
            $token=rand(1,100).''.time();
            $new_token = new Token;
            $new_token->token = $token;
            $new_token->admin_id = $admin[0]->id;
            $new_token->save();

            return response()->json(['status'=>1,'data'=>['token'=>$token]]);
        }
        else {
            return response()->json(['status'=>0,'data'=>[]]);
        };
    }
    //change password
    public function changePassword(Request $request)
    {
        $all=$request->all();
        //validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
            //return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
            return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
        }
        else
        {
            $admin=Token::where('token',$token)->get();
            $adminid=$admin[0]->admin_id;
            $admininfo=Admin::find($adminid);
            $db_old_password=$admininfo->password;
        }
        if(empty($all['old_password']))
		{
			$old_password='';
		}
		else
		{
			$old_password=$all['old_password'];
        }
        if(empty($all['new_password']))
		{
			$new_password='';
		}
		else
		{
			$new_password=$all['new_password'];
        }
        if(empty($all['confirm_password']))
		{
			$confirm_password='';
		}
		else
		{
			$confirm_password=$all['confirm_password'];
        }
        if(empty($old_password))
		{
            //return response()->json(['status'=>0,'message'=>'Please provide parameter old_password','data'=>[]]);
            return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro old_password','data'=>[]]);
        }
        if(empty($new_password))
		{
            //return response()->json(['status'=>0,'message'=>'Please provide parameter new_password','data'=>[]]);
            return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro new_password','data'=>[]]);
        }
        if(empty($confirm_password))
		{
            //return response()->json(['status'=>0,'message'=>'Please provide parameter confirm_password','data'=>[]]);
            return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro confirm_password','data'=>[]]);
        }
        if(md5($old_password)!=$db_old_password)
        {
            //return response()->json(['status'=>0,'message'=>'Old password does not match','data'=>[]]);
            return response()->json(['status'=>0,'message'=>'La vecchia password non corrisponde','data'=>[]]);
        }
        if($new_password!=$confirm_password)
        {
            //return response()->json(['status'=>0,'message'=>'New password and confirm password do not match','data'=>[]]);
            return response()->json(['status'=>0,'message'=>'La nuova password e la conferma della password non corrispondono','data'=>[]]);
        }
        else
        {
            $admininfo->password = md5($new_password);
            $admininfo->save();
            return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
        }
    }

    //change email
    public function changeEmail(Request $request)
    {
        $all=$request->all();
        //validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
            //return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
            return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
        }
        else
        {
            $admin=Token::where('token',$token)->get();
            $adminid=$admin[0]->admin_id;
            
        }
        if(empty($all['email']))
		{
			$email='';
		}
		else
		{
			$email=$all['email'];
        }
        if(empty($email))
		{
            //return response()->json(['status'=>0,'message'=>'Please provide parameter email','data'=>[]]);
            return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro email','data'=>[]]);
        }
        else
        {
            $admin_count=Admin::where('email',$email)->where('id','!=', $adminid)->count();
            if($admin_count==0){
                $admininfo=Admin::find($adminid);
                $admininfo->email = $email;
                $admininfo->save();
                return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
            }else{
                //return response()->json(['status'=>0,'message'=>'Email id already exist','data'=>[]]);
                return response()->json(['status'=>0,'message'=>'LID email esiste già','data'=>[]]);
            }
            
        }
    }

    //get user details
    public function getUserDetails(Request $request)
    {
        $all=$request->all();
        //validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
            //return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
            return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
        }
        else
        {
            $admin=Token::where('token',$token)->get();
            $adminid=$admin[0]->admin_id;
            
        }
        $admininfo = Admin::select('email')->where('id', '=', $adminid)->get();
    
        return response()->json(['status'=>1,'message'=>'success','data'=>$admininfo]);
		
	}
}
