<?php

namespace App\Modules\Common;
use App\Modules\Tokens\Models\Token;
use Illuminate\Http\Request;

class Common {
	
	//validate token for admin
	public static function validateAdminToken($token)
	{
		$token_count = Token::select('id','token')->where('token',$token)->count();
		return $token_count;
	}

}
