<?php

namespace App\Modules\Cities\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $incrementing = false;
    public function Municipality()
    {
        return $this->belongsToMany('App\Modules\Municipalities\Models\Municipality','name','name');
    }
}
