<?php

namespace App\Modules\MunicipalityTabs\Models;

use Illuminate\Database\Eloquent\Model;

class MunicipalityTab extends Model
{
    public function tab_type()
    {
        return $this->belongsTo('App\Modules\TabTypes\Models\TabType');
    }
	public function municipality()
    {
        return $this->belongsTo('App\Modules\Municipalities\Models\Municipality');
    }
}
