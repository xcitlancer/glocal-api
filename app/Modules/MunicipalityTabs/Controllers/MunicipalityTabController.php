<?php

namespace App\Modules\MunicipalityTabs\Controllers;

use App\Modules\MunicipalityTabs\Models\MunicipalityTab;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Common\Common;

class MunicipalityTabController extends Controller
{
	// get all municipality tabs for municipality  with pagination , serach
    public function getAllMunicipalityTabs(Request $request)
    {
		$all=$request->all();
		 
		if(empty($all['municipality_id']))
		{
			$municipality_id=0;
		}
		else
		{
			$municipality_id=$all['municipality_id'];
		}
		
		if(empty($all['page_no']) && empty($all['_page']))
		{
			$page_no=0;
		}
		else
		{
			$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
		}
		if(empty($all['no_of_rows'])&& empty($all['_limit']))
		{
			$no_of_rows=0;
		}else{
			$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
		}
		
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$offset = ($page_no - 1) * $no_of_rows;
		
		if(empty($page_no))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
		}
		if(empty($no_of_rows))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
		}
		$municipalitytab = MunicipalityTab::with(['tab_type','municipality']);
		$totalrows=MunicipalityTab::select('id','name');
		if(!empty($all['name_like']))
		{
			$name_like=$all['name_like'];
			$municipalitytab = $municipalitytab->where('name', 'LIKE', "$name_like%");
			$totalrows=$totalrows->where('name', 'LIKE', "$name_like%");
		}
		if(!empty($municipality_id))
		{
			$municipalitytab = $municipalitytab->where('municipality_id', $municipality_id);
			$totalrows=$totalrows->where('municipality_id', $municipality_id);
		}
		if(!empty($all['_sort']))
		{
			$sort_param=$all['_sort'];
		}
		else{
			$sort_param="id";
		}

		$municipalitytab = $municipalitytab->orderBy($sort_param,$order_type)->skip($offset)->take($no_of_rows)->get();
		$totalrows=$totalrows->count();
		
        return response()->json(['status'=>1,'message'=>'success','totalrows'=>$totalrows,'data'=>$municipalitytab]);
    }
	// municipality tab add
	public function municipalityTabAdd(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['municipality_id']))
		{
			$municipality_id=0;
			
		}
		else
		{
			$municipality_id=$all['municipality_id'];
		}
		if(empty($all['tab_name']))
		{
			$tab_name='';
			
		}
		else
		{
			$tab_name=$all['tab_name'];
		}
		if(empty($all['tab_type_id']))
		{
			$tab_type_id=0;
		}
		else
		{
			$tab_type_id=$all['tab_type_id'];
		}
		if(empty($municipality_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input municipality_id','data'=>[]]);
		}
		if(empty($tab_name))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter tab_name','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input tab_name','data'=>[]]);
		}
		else if(empty($tab_type_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter tab_type_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input tab_type_id','data'=>[]]);
		}
		
		else
		{
			
			$municipality_tab = new MunicipalityTab;
			$municipality_tab->name = $tab_name;
			$municipality_tab->municipality_id  = $municipality_id;
			$municipality_tab->tab_type_id  = $tab_type_id;
			$municipality_tab->status  = 1;
			$municipality_tab->save();
				
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}
	
	//municipality tab  details
	public function getMunicipalityTabDetails($tab_id)
    {
		$municipality_tab_count = MunicipalityTab::select('id')->where('id', $tab_id)->count();
		if($municipality_tab_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else
		{
			$municipality_tab = MunicipalityTab::with(['tab_type','municipality'])->where('id', $tab_id)->get();
			return response()->json(['status'=>1,'message'=>'success','data'=>$municipality_tab]);
		}
	}
	
	//municipality tab edit
	public function municipalityTabEdit(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['tab_id']))
		{
			$tab_id=0;
			
		}
		else
		{
			$tab_id=$all['tab_id'];
		}
		if(empty($all['tab_name']))
		{
			$tab_name='';
			
		}
		else
		{
			$tab_name=$all['tab_name'];
		}
		
		if(empty($tab_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter tab_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input tab_id','data'=>[]]);
		}
		if(empty($tab_name))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter tab_name','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input tab_name','data'=>[]]);
		}
		else
		{
			$municipality_tab = MunicipalityTab::find($tab_id);
			$municipality_tab->name = $tab_name;
			$municipality_tab->save();
				
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}
	//municipality tab delete
	public function municipalityTabDelete(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['tab_id']))
		{
			$tab_id=0;
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter tab_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input tab_id','data'=>[]]);
			
		}
		else
		{
			$tab_id=$all['tab_id'];
		}
		$municipality_tab_count = MunicipalityTab::select('id')->where('id', $tab_id)->count();
			
			if($municipality_tab_count>0)
			{
				MunicipalityTab::select('id')->where('id', $tab_id)->delete();
				return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
			}
			else
			{
				//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
				return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
			}
	}
	
	//get municipality tabs for front end
	public function getMunicipalityTabs(Request $request)
    {
		$all=$request->all();
		 
		if(empty($all['municipality_id']))
		{
			$municipality_id=0;
		}
		else
		{
			$municipality_id=$all['municipality_id'];
		}
		if(empty($municipality_id))
		{
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input municipality_id','data'=>[]]);
		}

		$municipalitytab = MunicipalityTab::with(['tab_type','municipality']);
		$totalrows=MunicipalityTab::select('id','name');
		if(!empty($municipality_id))
		{
			$municipalitytab = $municipalitytab->where('municipality_id', $municipality_id);
			$totalrows=$totalrows->where('municipality_id', $municipality_id);
		}
		if(!empty($all['_sort']))
		{
			$sort_param=$all['_sort'];
		}
		else{
			$sort_param="name";
		}
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$municipalitytab = $municipalitytab->orderBy($sort_param,$order_type)->get();
		$totalrows=$totalrows->count();
		
        return response()->json(['status'=>1,'message'=>'success','totalrows'=>$totalrows,'data'=>$municipalitytab]);
    }
}
