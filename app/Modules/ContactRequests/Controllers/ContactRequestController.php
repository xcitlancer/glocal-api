<?php

namespace App\Modules\ContactRequests\Controllers;
use App\Modules\Common\Common;
use App\Modules\ContactRequests\Models\ContactRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactRequestController extends Controller
{
	// get all contact request with pagination , serach
    public function getAllContactRequests(Request $request)
    {
		$all=$request->all();
		
		if(empty($all['page_no']) && empty($all['_page']))
		{
			$page_no=0;
		}
		else
		{
			$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
		}
		if(empty($all['no_of_rows'])&& empty($all['_limit']))
		{
			$no_of_rows=0;
		}else{
			$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
		}
		
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="desc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		
		$offset = ($page_no - 1) * $no_of_rows;
		
		if(empty($page_no))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
		}
		if(empty($no_of_rows))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
		}
		
		
		$contactrequests = ContactRequest::select('name','id','email','designation','message','is_read','created_at')->where('id','>' ,0);
		$totalrows= ContactRequest::where('id','>' ,0);
		
		if(!empty($all['name_like']))
		{
			$name_like=$all['name_like'];
			$contactrequests = $contactrequests->where('name', 'LIKE', "$name_like%");
			$totalrows= $totalrows->where('name', 'LIKE', "$name_like%");
		}
		if(!empty($all['email_like']))
		{
			$email_like=$all['email_like'];
			$contactrequests = $contactrequests->where('email', 'LIKE', "$email_like%");
			$totalrows= $totalrows->where('email', 'LIKE', "$email_like%");
		}
		if(!empty($all['_sort']))
		{
			$sort_param=$all['_sort'];
		}
		else{
			$sort_param="id";
		}

		$contactrequests = $contactrequests->orderBy($sort_param,$order_type)->skip($offset)->take($no_of_rows)->get();
		$totalrows= $totalrows->count();
		$all_row_status_change = ContactRequest::where('id','>',0)->orderBy('id',$order_type)->skip($offset)->take($no_of_rows)->update(array('is_read' => 1));
        return response()->json(['status'=>1,'message'=>'success','totalrows'=>$totalrows,'data'=>$contactrequests]);
	}
	
	// add contact request
	
	public function contactRequestsAdd(Request $request)
    {
		
		$all=$request->all();
		
		if(empty($all['name']))
		{
			$name='';
			
		}
		else
		{
			$name=$all['name'];
		}
		
		if(empty($all['email']))
		{
			$email='';
			
		}
		else
		{
			$email=$all['email'];
		}
		if(empty($all['message']))
		{
			$message='';
			
		}
		else
		{
			$message=$all['message'];
		}
		if(empty($all['designation']))
		{
			$designation='';
			
		}
		else
		{
			$designation=$all['designation'];
		}
		
		if(empty($name))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input name','data'=>[]]);
		}
		if (!preg_match("/^[a-zA-Z-' ]*$/",$name)) {
			//$nameErr = "Only letters and white space allowed";
			return response()->json(['status'=>0,'message'=>'Solo lettere e spazi bianchi sono consentiti in name','data'=>[]]);
		}
		if(empty($email))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input email','data'=>[]]);
		}
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			//$emailErr = "Invalid email format";
			return response()->json(['status'=>0,'message'=>'e-mail non valido','data'=>[]]);
		}
		if(empty($message))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input message','data'=>[]]);
		}
		if(empty($designation))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input designation','data'=>[]]);
		}
		else
		{
			$contactrequest = new ContactRequest;
			$contactrequest->name = $name;
			$contactrequest->email = $email;
			$contactrequest->message = $message;
			$contactrequest->is_read = 0;
			$contactrequest->designation = $designation;
			
			$contactrequest->save();	
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}
}
