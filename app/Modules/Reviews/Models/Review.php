<?php

namespace App\Modules\Reviews\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function municipality()
    {
        return $this->belongsTo('App\Modules\Municipalities\Models\Municipality');
    }
}
