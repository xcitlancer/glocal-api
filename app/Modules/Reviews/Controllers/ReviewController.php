<?php

namespace App\Modules\Reviews\Controllers;
use App\Modules\Common\Common;
use App\Modules\Reviews\Models\Review;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReviewController extends Controller
{
	// get all reviews with pagination , serach
    public function getAllReviews(Request $request)
    {
		$all=$request->all();
		
		if(empty($all['page_no']) && empty($all['_page']))
		{
			$page_no=0;
		}
		else
		{
			$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
		}
		if(empty($all['no_of_rows'])&& empty($all['_limit']))
		{
			$no_of_rows=0;
		}else{
			$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
		}
		
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		if(empty($all['municipality_id']))
		{
			
			$municipality_id=0;
		}
		else{
			$municipality_id=$all['municipality_id'];
		}
		if(isset($all['is_active']))
		{
			
			$is_active=$all['is_active'];
		}
		else{
			$is_active="";
		}
		$offset = ($page_no - 1) * $no_of_rows;
		
		if(empty($page_no))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
		}
		if(empty($no_of_rows))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
		}
		if(empty($municipality_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro municipality_id','data'=>[]]);
		}
		
		$reviews = Review::select(DB::raw("CONCAT(first_name,' ',middle_name,' ',last_name) AS name"),'id','email','review_txt','is_active','fb_user_id','created_at','municipality_id')->with(['municipality'])->where('municipality_id', $municipality_id);
		$totalrows= Review::where('municipality_id', $municipality_id);
		
		if(!empty($all['name_like']))
		{
			$name_like=$all['name_like'];
			$reviews = $reviews->where('first_name', 'LIKE', "$name_like%");
			$totalrows= $totalrows->where('first_name', 'LIKE', "$name_like%");
		}
		if(!empty($all['email_like']))
		{
			$email_like=$all['email_like'];
			$reviews = $reviews->where('email', 'LIKE', "$email_like%");
			$totalrows= $totalrows->where('email', 'LIKE', "$email_like%");
		}
		if($is_active!="")
		{
			
			$reviews = $reviews->where('is_active', $is_active);
			$totalrows= $totalrows->where('is_active', $is_active);
		}
		if(!empty($all['_sort']))
		{
			$sort_param=$all['_sort'];
		}
		else{
			$sort_param="id";
		}

		$reviews = $reviews->orderBy($sort_param,$order_type)->skip($offset)->take($no_of_rows)->get();
		$totalrows= $totalrows->count();
		
        return response()->json(['status'=>1,'message'=>'success','totalrows'=>$totalrows,'data'=>$reviews]);
	}
	
	//change municipality review status

	public function reviewStatusChange(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['review_id']))
		{
			$review_id=0;
			
		}
		else
		{
			$review_id=$all['review_id'];
		}
		
		if(empty($review_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter review_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input review_id','data'=>[]]);
		}
		else
		{
			$reviewdata=Review::find($review_id);
			if($reviewdata->is_active==0)
			{
				$is_active_update=1;
			}else{
				$is_active_update=0;
			}
			$reviewdata->is_active = $is_active_update;
			$reviewdata->save();
				
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}

	//review add api
	public function reviewAdd(Request $request)
    {
		
		$all=$request->all();
		
		if(empty($all['municipality_id']))
		{
			$municipality_id=0;
			
		}
		else
		{
			$municipality_id=$all['municipality_id'];
		}
		if(empty($all['first_name']))
		{
			$first_name='';
			
		}
		else
		{
			$first_name=$all['first_name'];
		}
		if(empty($all['middle_name']))
		{
			$middle_name='';
			
		}
		else
		{
			$middle_name=$all['middle_name'];
		}
		if(empty($all['last_name']))
		{
			$last_name='';
			
		}
		else
		{
			$last_name=$all['last_name'];
		}
		if(empty($all['email']))
		{
			$email='';
			
		}
		else
		{
			$email=$all['email'];
		}
		if(empty($all['review_txt']))
		{
			$review_txt='';
			
		}
		else
		{
			$review_txt=$all['review_txt'];
		}
		if(empty($all['fb_user_id']))
		{
			$fb_user_id='';
			
		}
		else
		{
			$fb_user_id=$all['fb_user_id'];
		}
		if(empty($municipality_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input municipality_id','data'=>[]]);
		}
		if(empty($first_name))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input first_name','data'=>[]]);
		}
		if (!preg_match("/^[a-zA-Z-' ]*$/",$first_name)) {
			//$nameErr = "Only letters and white space allowed";
			return response()->json(['status'=>0,'message'=>'Solo lettere e spazi bianchi sono consentiti in first_name','data'=>[]]);
		}
		if(!empty($middle_name)){
			if (!preg_match("/^[a-zA-Z-' ]*$/",$middle_name)) {
				//$nameErr = "Only letters and white space allowed";
				return response()->json(['status'=>0,'message'=>'Solo lettere e spazi bianchi sono consentiti in middle_name','data'=>[]]);
			}
		}
		if(empty($last_name))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input last_name','data'=>[]]);
		}
		if (!preg_match("/^[a-zA-Z-' ]*$/",$last_name)) {
			//$nameErr = "Only letters and white space allowed";
			return response()->json(['status'=>0,'message'=>'Solo lettere e spazi bianchi sono consentiti in last_name','data'=>[]]);
		}
		if(empty($email))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input email','data'=>[]]);
		}
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			//$emailErr = "Invalid email format";
			return response()->json(['status'=>0,'message'=>'e-mail non valido','data'=>[]]);
		}
		if(empty($review_txt))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input review_txt','data'=>[]]);
		}
		
		else
		{
			$review = new Review;
			$review->municipality_id = $municipality_id;
			$review->first_name = $first_name;
			if(!empty($middle_name)){
				$review->middle_name = $middle_name;
			}
			$review->last_name = $last_name;
			$review->email = $email;
			$review->review_txt = $review_txt;
			$review->is_active = 1;
			if(!empty($fb_user_id)){
				$review->fb_user_id = $fb_user_id;
			}
			$review->save();	
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}

}
