<?php

namespace App\Modules\MunicipalityTabAttributes\Controllers;

use App\Modules\MunicipalityTabAttributes\Models\MunicipalityTabAttribute;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Common\Common;

class MunicipalityTabAttributeController extends Controller
{
	// municipality tab attributes add
	public function municipalityTabAttributeAdd(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['tab_id']))
		{
			$tab_id=0;
			
		}
		else
		{
			$tab_id=$all['tab_id'];
		}
		if(empty($all['myformdata']))
		{
			$myformdata='';
			
		}
		else
		{
			$myformdata=$all['myformdata'];
		}
		
		if(empty($tab_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter tab_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input tab_id','data'=>[]]);
		}
		if(empty($myformdata))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input tab attributes','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Pfornire gli attributi della scheda di input','data'=>[]]);
		}
		else
		{
			$municipality_tab_attributes_count = MunicipalityTabAttribute::select('id')->where('municipality_tab_id', $tab_id)->count();
			
			//first delete old record then insert new record
			if($municipality_tab_attributes_count>0)
			{
				MunicipalityTabAttribute::select('id')->where('municipality_tab_id', $tab_id)->delete();
			}
			
			//return response()->json(['status'=>1,'message'=>'success','data'=>$myformdata]);
			foreach($myformdata['dynamicfields'] as $val)
			{
				$formcontrolcontent=$val['formcontrolcontent'];
				$formcontrolcontent=str_ireplace('<p>','',$formcontrolcontent);
				$formcontrolcontent=str_ireplace('</p>','',$formcontrolcontent);
				$formcontrolpercentage=$val['formcontrolpercentage'];
				$formcontrolposition=$val['formcontrolposition'];
				$formcontroltitle=$val['formcontroltitle'];
				$formcontroltitle=str_ireplace('<p>','',$formcontroltitle);
				$formcontroltitle=str_ireplace('</p>','',$formcontroltitle);   
				$formcontrolattrtype=$val['formcontrolattrtype'];
				$formcontrolapibased=$val['formcontrolapibased'];
				$formcontrolorder=$val['formcontrolorder'];


				if(empty($formcontrolpercentage)){
					$formcontrolpercentage=0;
				}
				if(empty($formcontrolposition)){
					$formcontrolposition='L';
				}
				
				if($formcontrolattrtype==1){
					$formcontroltitle="";
					$formcontrolcontent="";
					$formcontrolpercentage=0;
				}
				if($formcontrolattrtype==2){
					$formcontrolapibased="";
				}

				$municipalitytabattribute = new MunicipalityTabAttribute;
				$municipalitytabattribute->municipality_tab_id  = $tab_id;
				$municipalitytabattribute->title = $formcontroltitle;
				$municipalitytabattribute->content  = $formcontrolcontent;
				$municipalitytabattribute->position  = $formcontrolposition;
				$municipalitytabattribute->percentage  = $formcontrolpercentage;
				if(!empty($formcontrolattrtype)){
					$municipalitytabattribute->attribute_type  = $formcontrolattrtype;	
				}
				if(!empty($formcontrolapibased)){
					$municipalitytabattribute->api_based  = $formcontrolapibased;
				}

				//change title for api based
				$formcontrolweathertitle=$val['formcontrolweathertitle'];
				$formcontrolairqualitytitle=$val['formcontrolairqualitytitle'];
				
				if(!empty($formcontrolattrtype) && !empty($formcontrolapibased)){
					if($formcontrolattrtype==1)
					{
						
						if($formcontrolapibased=="Weather"){
							$municipalitytabattribute->title  = $formcontrolweathertitle;
						}
						else if($formcontrolapibased=="Air Quality"){
							//return response()->json(['status'=>1,'message'=>'success','data'=>$formcontrolairqualitytitle]);
							$municipalitytabattribute->title  = $formcontrolairqualitytitle;
						}
					}
				}
				$municipalitytabattribute->attribute_order  = $formcontrolorder;
				$municipalitytabattribute->save();
			}
		
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}
	
	//admin panel municipality tab attributes details
	public function getMunicipalityTabAttribute($tab_id)
    {
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$municipality_tab_attributes_count = MunicipalityTabAttribute::select('id')->where('municipality_tab_id', $tab_id)->count();
		if($municipality_tab_attributes_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else
		{
			$municipality_tab_attributes = MunicipalityTabAttribute::with('municipality_tab','municipality_tab.municipality.weather','municipality_tab.municipality.airquality')->where('municipality_tab_id', $tab_id)->orderBy('attribute_order',$order_type)->get();
			return response()->json(['status'=>1,'message'=>'success','data'=>$municipality_tab_attributes]);
		}
	}

	// update attribute order status

	public function tabAttributeUpdateOrder(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['attribute_id']))
		{
			$attribute_id=0;
			
		}
		else
		{
			$attribute_id=$all['attribute_id'];
		}
		if(empty($all['attribute_order']))
		{
			$attribute_order='';
			
		}
		else
		{
			$attribute_order=$all['attribute_order'];
		}
		
		if(empty($attribute_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input attribute_id','data'=>[]]);
		}
		if(empty($attribute_order))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter tab_name','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input attribute_order','data'=>[]]);
		}
		else
		{
			$municipality_tab_attr = MunicipalityTabAttribute::find($attribute_id);
			$municipality_tab_attr->attribute_order = $attribute_order;
			$municipality_tab_attr->save();
				
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
			
		}
	}

	//front end municipality tab attribute details api
	public function getMunicipalityTabAttributeInfo($tab_id)
    {
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$municipality_tab_attributes_count = MunicipalityTabAttribute::select('id')->where('municipality_tab_id', $tab_id)->count();
		if($municipality_tab_attributes_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else
		{
			$municipality_tab_attributes = MunicipalityTabAttribute::with('municipality_tab','municipality_tab.municipality.weather','municipality_tab.municipality.airquality')->where('municipality_tab_id', $tab_id)->orderBy('attribute_order',$order_type)->get();
			//return response()->json(['status'=>1,'message'=>'success','data'=>$municipality_tab_attributes]);
			$tab_attributes_data=[];
			foreach($municipality_tab_attributes as $key_name=>$value)
			{
				if($value['title']=="Weather (now)")
				{
					$obj=json_decode($value->municipality_tab->municipality->weather->weather);
					$content=$obj->main->temp."°C + Humidity (".$obj->main->humidity."%) = feels ".$obj->main->feels_like."°C";
					$municipality_tab_attributes[$key_name]->icon=$obj->weather[0]->icon;
				}
				else if($value['title']=="Humidity (now)")
				{
					//Sweaty: 83%
					$obj=json_decode($value->municipality_tab->municipality->weather->weather);
					$content=$obj->main->humidity.'%';
					$municipality_tab_attributes[$key_name]->icon=$obj->weather[0]->icon;
				}
				else if($value['title']=="Temperature (now)")
				{
					$obj=json_decode($value->municipality_tab->municipality->weather->weather);
					//Perfect: 28°C (feels 32°C)
					$content=$obj->main->temp."°C (feels ".$obj->main->feels_like."°C)";
					$municipality_tab_attributes[$key_name]->icon=$obj->weather[0]->icon;
				}
				else if($value['title']=="Air quality (now)")
				{
					//Great: 49 US AQI
					$obj=json_decode($value->municipality_tab->municipality->airquality->airquality_details);
					$content=$obj->data->aqi." US AQI";
					//$municipality_tab_attributes[$key_name]->icon=$obj->weather[0]->icon;
				}
				else{
					$content=$value["content"];
					$municipality_tab_attributes[$key_name]->icon="";
				}
				$municipality_tab_attributes[$key_name]->content=$content;
				
				$municipality_tab_attributes[$key_name]->municipality=$municipality_tab_attributes[$key_name]->municipality_tab->municipality;
				unset($municipality_tab_attributes[$key_name]->municipality->weather);
				unset($municipality_tab_attributes[$key_name]->municipality->airquality);
				unset($municipality_tab_attributes[$key_name]->municipality_tab->municipality);
			}
			return response()->json(['status'=>1,'message'=>'success','data'=>$municipality_tab_attributes]);
		}
	}
	
}
