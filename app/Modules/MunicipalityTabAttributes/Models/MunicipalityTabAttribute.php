<?php

namespace App\Modules\MunicipalityTabAttributes\Models;

use Illuminate\Database\Eloquent\Model;

class MunicipalityTabAttribute extends Model
{
    public function municipality_tab()
    {
        return $this->belongsTo('App\Modules\MunicipalityTabs\Models\MunicipalityTab');
    }
	
}
