<?php

namespace App\Modules\Airqualities\Models;

use Illuminate\Database\Eloquent\Model;

class Airquality extends Model {
    protected $fillable = ['municipality_id'];

    public function municipality() {
        return $this->belongsTo('App\Modules\Municipalities\Models\Municipality');
    }
}
