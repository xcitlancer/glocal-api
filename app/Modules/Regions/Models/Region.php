<?php

namespace App\Modules\Regions\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public function provinces()
    {
        return $this->hasMany('App\Modules\Provinces\Models\Province');
    }
    public function municipalities()
    {
        return $this->hasMany('App\Modules\Municipalities\Models\Municipality');
    }
}
