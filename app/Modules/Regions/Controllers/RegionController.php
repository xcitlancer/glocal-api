<?php

namespace App\Modules\Regions\Controllers;
use App\Modules\Common\Common;
use App\Modules\Regions\Models\Region;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;

class RegionController extends Controller
{
	// get all regions with pagination , serach
    public function getAllRegions(Request $request)
    {
		$all=$request->all();
		
		if(empty($all['page_no']) && empty($all['_page']))
		{
			$page_no=0;
		}
		else
		{
			$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
		}
		if(empty($all['no_of_rows'])&& empty($all['_limit']))
		{
			$no_of_rows=0;
		}else{
			$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
		}
		
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$offset = ($page_no - 1) * $no_of_rows;
		
		if(empty($page_no))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
		}
		if(empty($no_of_rows))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
		}
		
		if(empty($all['name_like']))
		{
			$regions = Region::select('id','name','region_img')->orderBy('name',$order_type)->skip($offset)->take($no_of_rows)->get();
			$totalrows=Region::select('id','name')->count();
		}
		else{
			$name_like=$all['name_like'];
			$regions = Region::select('id','name','region_img')->where('name', 'LIKE', "$name_like%")->orderBy('name',$order_type)->skip($offset)->take($no_of_rows)->get();
			
			$totalrows=Region::select('id','name')->where('name', 'LIKE', "$name_like%")->count();
		}
        return response()->json(['status'=>1,'message'=>'success','totalrows'=>$totalrows,'data'=>$regions]);
    }
	// get all regions
	public function getAllRegionsList()
    {
		$regions_count = Region::select('id','name')->orderBy('name','asc')->count();
		if($regions_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else{
			$regions = Region::select('id','name','region_img')->orderBy('name','asc')->get();
			return response()->json(['status'=>1,'message'=>'success','data'=>$regions,'thumbnail_folder_path'=>'regionimages/thumbnail','medium_folder_path'=>'regionimages/medium','original_folder_path'=>'regionimages/original',]);
		}
	}
	//get region details by region id
	public function getRegionDetails($region_id)
	{
		$region_count = Region::select('id','name','region_img')->where('id', $region_id)->count();
		if($region_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else
		{
			$region = Region::select('id','name','region_img')->where('id', $region_id)->get();
			return response()->json(['status'=>1,'message'=>'success','data'=>$region,'thumbnail_folder_path'=>'regionimages/thumbnail','medium_folder_path'=>'regionimages/medium','original_folder_path'=>'regionimages/original']);
		}
	}
	//region edit
	public function regionEditByID(Request $request)
    {
		//validate token
		
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		 if ($request->hasFile('image_name'))
		{
			//return response()->json(['status'=>1,'message'=>'image got','data'=>[]]);
		}
		else{
			//return response()->json(['status'=>0,'message'=>'missing image','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'immagine mancante','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['id']))
		{
			$id=0;
			
		}
		else
		{
			$id=$all['id'];
		}
		if(empty($all['image_name']))
		{
			$image_name='';
		}
		else
		{
			$image_name=$all['image_name'];
		}
		if(empty($id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input id','data'=>[]]);
		}
		else if(empty($image_name))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter image_name. Nothing to update.','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input image_name. Niente da aggiornare.','data'=>[]]);
		}
		else
		{
			$rules = array(
			'image' => 'mimes:jpeg,jpg,png|required|max:100000' // max 100000kb
			);
			$postData = $request->only('image_name');
			$file = $postData['image_name'];
			$fileArray = array('image' => $file);

			$validator = Validator::make($fileArray, $rules);


			if ($validator->fails()) 
			{
				//return response()->json(['status'=>0,'message'=>'Please upload a valid image with max size 100 mb.','data'=>[]]);
				return response()->json(['status'=>0,'message'=>'Carica unimmagine valida con una dimensione massima di 100 MB.','data'=>[]]);
			}
			
			$image = $request->file('image_name');	
			$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
			#image resize
			#thumbnail
			Image::make($request->file('image_name')->getRealPath())->resize(200, 200)->save( public_path('regionimages/thumbnail/' . $input['imagename']) );
			
			#medium
			Image::make($request->file('image_name')->getRealPath())->resize(300, 300)->save( public_path('regionimages/medium/' . $input['imagename']) );
	
			#original image
			$request->image_name->move(public_path('regionimages/original'), $input['imagename']);
		
			$region = Region::find($id);
			//unlink images from folder
			$image_db_name=$region->region_img;
			
				if(!empty($image_db_name)){
					if (file_exists(public_path()."/regionimages/thumbnail/".$image_db_name)) {
						$path = public_path()."/regionimages/thumbnail/".$image_db_name;
						unlink($path);
					}
					if (file_exists(public_path()."/regionimages/medium/".$image_db_name)) {
						$path = public_path()."/regionimages/medium/".$image_db_name;
						unlink($path);
					}
					if (file_exists(public_path()."/regionimages/original/".$image_db_name)) {
						$path = public_path()."/regionimages/original/".$image_db_name;
						unlink($path);
					}

				}
			
			//update new image
			$region->region_img = $input['imagename'];
			$region->save();
			
			if(empty($input['imagename']))
			{
				$imagename_output=[];
			}
			else
			{
				$imagename_output=[ "imagename" => $input['imagename'] ];
			}
			
			return response()->json(['status'=>1,'message'=>'success','data'=>$imagename_output,'thumbnail_folder_path'=>'regionimages/thumbnail','medium_folder_path'=>'regionimages/medium','original_folder_path'=>'regionimages/original']);
		}
	}
}
