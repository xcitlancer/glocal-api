<?php

namespace App\Modules\Provinces\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model 
{
    public function region()
    {
        return $this->belongsTo('App\Modules\Regions\Models\Region');
    }
    public function municipalities()
    {
        return $this->hasMany('App\Modules\Municipalities\Models\Municipality');
    }
}
