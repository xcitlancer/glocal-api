<?php

namespace App\Modules\Provinces\Controllers;
use App\Modules\Common\Common;
use App\Modules\Provinces\Models\Province;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Regions\Models\Region;
use Image;
use Illuminate\Support\Facades\Validator;
class ProvinceController extends Controller
{
	// get province list by region id with pagination , serach
    public function getProvincesByRegion(Request $request)
    {
		/*
        $provinces = Province::whereHas('region',function($q) use ($regionCode) {
            $q->where('code', $regionCode);
        })->get();*/
		
		//$provinces = Region::with(['provinces'])->where('id',$region_code)->get();
		
		$all=$request->all();
		
		if(empty($all['region_id']))
		{
			$region_id=0;
		}
		else
		{
			$region_id=$all['region_id'];
		}
		
		if(empty($all['page_no']) && empty($all['_page']))
		{
			$page_no=0;
		}
		else
		{
			$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
		}
		if(empty($all['no_of_rows'])&& empty($all['_limit']))
		{
			$no_of_rows=0;
		}else{
			$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
		}
		
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$offset = ($page_no - 1) * $no_of_rows;
		
		if(empty($page_no))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
		}
		if(empty($no_of_rows))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
		}
		
		$totalrows=Province::select('id','name');
		$provinces = Province::with(['region']);
		//making dynamic query
		if(!empty($region_id))
		{
			$provinces=$provinces->where('region_id',$region_id);
			$totalrows=$totalrows->where('region_id',$region_id);
		}
		if(!empty($all['name_like']))
		{
			$name_like=$all['name_like'];
			$provinces=$provinces->where('name', 'LIKE', "$name_like%");
			$totalrows=$totalrows->where('name', 'LIKE', "$name_like%");
		}
		if(!empty($all['region_like']))
		{
			$region_name=$all['region_like'];
			$provinces=$provinces->whereHas('region',function($q) use ($region_name) {
					$q->where('name', 'LIKE', "$region_name%");
				});
			$totalrows=$totalrows->whereHas('region',function($q) use ($region_name) {
					$q->where('name', 'LIKE', "$region_name%");
				});	
		}
		$provinces=$provinces->orderBy('name',$order_type)->skip($offset)->take($no_of_rows)->get();
		$totalrows=$totalrows->count();
				
        return response()->json(['status'=>1,'message'=>'success','data'=>$provinces,'thumbnail_folder_path'=>'provinceimages/thumbnail','medium_folder_path'=>'provinceimages/medium','original_folder_path'=>'provinceimages/original','totalrows'=>$totalrows]);
    }
	//get province details by province id
	public function getProvincesDetails($province_id)
    {
		$provinces_count = Province::select('id','name','province_img')->where('id', $province_id)->count();
		if($provinces_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else
		{
			$provinces = Province::with(['region'])->where('id', $province_id)->get();
			return response()->json(['status'=>1,'message'=>'success','data'=>$provinces,'thumbnail_folder_path'=>'provinceimages/thumbnail','medium_folder_path'=>'provinceimages/medium','original_folder_path'=>'provinceimages/original']);
		}
	}
	// edit image province 
	public function provinceEditByID(Request $request)
    {
		//validate token
		
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		 if ($request->hasFile('image_name'))
		{
			//return response()->json(['status'=>1,'message'=>'image got','data'=>[]]);
		}
		else{
			//return response()->json(['status'=>0,'message'=>'missing image','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'immagine mancante','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['id']))
		{
			$id=0;
			
		}
		else
		{
			$id=$all['id'];
		}
		if(empty($all['image_name']))
		{
			$image_name='';
		}
		else
		{
			$image_name=$all['image_name'];
		}
		if(empty($id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input id','data'=>[]]);
		}
		else if(empty($image_name))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter image_name. Nothing to update.','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input image_name. Niente da aggiornare.','data'=>[]]);
		}
		else
		{
			$rules = array(
			'image' => 'mimes:jpeg,jpg,png|required|max:100000' // max 100000kb
			);
			$postData = $request->only('image_name');
			$file = $postData['image_name'];
			$fileArray = array('image' => $file);

			$validator = Validator::make($fileArray, $rules);


			if ($validator->fails()) 
			{
				//return response()->json(['status'=>0,'message'=>'Please upload a valid image with max size 100 mb.','data'=>[]]);
				return response()->json(['status'=>0,'message'=>'Carica unimmagine valida con una dimensione massima di 100 MB.','data'=>[]]);
			}
			
			$image = $request->file('image_name');	
			$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
			#image resize
			#thumbnail
			Image::make($request->file('image_name')->getRealPath())->resize(200, 200)->save( public_path('provinceimages/thumbnail/' . $input['imagename']) );
			
			#medium
			Image::make($request->file('image_name')->getRealPath())->resize(300, 300)->save( public_path('provinceimages/medium/' . $input['imagename']) );
	
			#original image
			$request->image_name->move(public_path('provinceimages/original'), $input['imagename']);
		
			$province = Province::find($id);
			//unlink images from folder
			$image_db_name=$province->province_img;
			
				if(!empty($image_db_name)){
					if (file_exists(public_path()."/provinceimages/thumbnail/".$image_db_name)) {
						$path = public_path()."/provinceimages/thumbnail/".$image_db_name;
						unlink($path);
					}
					if (file_exists(public_path()."/provinceimages/medium/".$image_db_name)) {
						$path = public_path()."/provinceimages/medium/".$image_db_name;
						unlink($path);
					}
					if (file_exists(public_path()."/provinceimages/original/".$image_db_name)) {
						$path = public_path()."/provinceimages/original/".$image_db_name;
						unlink($path);
					}

				}
			
			//update new image
			$province->province_img = $input['imagename'];
			$province->save();
			
			if(empty($input['imagename']))
			{
				$imagename_output=[];
			}
			else
			{
				$imagename_output=[ "imagename" => $input['imagename'] ];
			}
			
			return response()->json(['status'=>1,'message'=>'success','data'=>$imagename_output,'thumbnail_folder_path'=>'provinceimages/thumbnail','medium_folder_path'=>'provinceimages/medium','original_folder_path'=>'provinceimages/original']);
		}
	}
}
