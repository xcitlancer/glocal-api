<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MunicipalityPropertyValue
 *
 * @author sucha
 */

namespace App\Modules\MunicipalityPropertyValues\Models;

use Illuminate\Database\Eloquent\Model;

class MunicipalityPropertyValue extends Model{
    //put your code here
    function municipality() {
        return $this->belongsTo('App\Modules\Municipalities\Models\Municipality');
    }
}
