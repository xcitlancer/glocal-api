<?php

namespace App\Modules\MunicipalityImages\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Common\Common;
use App\Modules\MunicipalityImages\Models\MunicipalityImage;

class MunicipalityImageController extends Controller
{
	// get all municipality images with pagination , serach
    public function getAllMunicipalityImages(Request $request)
    {
		$all=$request->all();
		 
		if(empty($all['municipality_id']))
		{
			$municipality_id=0;
		}
		else
		{
			$municipality_id=$all['municipality_id'];
		}
		
		if(empty($all['page_no']) && empty($all['_page']))
		{
			$page_no=0;
		}
		else
		{
			$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
		}
		if(empty($all['no_of_rows'])&& empty($all['_limit']))
		{
			$no_of_rows=0;
		}else{
			$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
		}
		
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$offset = ($page_no - 1) * $no_of_rows;
		
		if(empty($page_no))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
		}
		if(empty($no_of_rows))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
		}
		$municipalityimage = MunicipalityImage::with(['municipality']);
		$totalrows=MunicipalityImage::select('id','name');
		
		if(!empty($municipality_id))
		{
			$municipalityimage = $municipalityimage->where('municipality_id', $municipality_id);
			$totalrows=$totalrows->where('municipality_id', $municipality_id);
		}
		
		if(!empty($all['_sort']))
		{
			$sort_param=$all['_sort'];
		}
		else{
			$sort_param="id";
		}

		$municipalityimage = $municipalityimage->orderBy($sort_param,$order_type)->skip($offset)->take($no_of_rows)->get();
		$totalrows=$totalrows->count();
		
        return response()->json(['status'=>1,'message'=>'success','totalrows'=>$totalrows,'data'=>$municipalityimage]);
    }
	//delete municipality images from image gallery
	public function municipalityImageDelete(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['image_id']))
		{
			$image_id=0;
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter image_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input image_id','data'=>[]]);
			
		}
		else
		{
			$image_id=$all['image_id'];
		}
		$municipality_image_count = MunicipalityImage::select('id')->where('id', $image_id)->count();
			
			if($municipality_image_count>0)
			{
				$image_db_name=MunicipalityImage::select('id','img')->where('id', $image_id)->first()->img;
				//unlink images from folder

				if(!empty($image_db_name))
				{
					if (file_exists(public_path()."/municipalityimages/thumbnail/".$image_db_name)) {
						$path = public_path()."/municipalityimages/thumbnail/".$image_db_name;
						unlink($path);
					}
					if (file_exists(public_path()."/municipalityimages/medium/".$image_db_name)) {
						$path = public_path()."/municipalityimages/medium/".$image_db_name;
						unlink($path);
					}
					if (file_exists(public_path()."/municipalityimages/original/".$image_db_name)) {
						$path = public_path()."/municipalityimages/original/".$image_db_name;
						unlink($path);
					}
				}
				
				MunicipalityImage::select('id')->where('id', $image_id)->delete();
				return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
			}
			else
			{
				//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
				return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
			}
	}
	//set primary image for gallery
	public function municipalityImageSetPrimary(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['image_id']))
		{
			$image_id=0;
			
		}
		else
		{
			$image_id=$all['image_id'];
		}
		if(empty($all['municipality_id']))
		{
			$municipality_id=0;
			
		}
		else
		{
			$municipality_id=$all['municipality_id'];
		}
		
		if(empty($image_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter image_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input image_id','data'=>[]]);
		}
		if(empty($municipality_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input municipality_id','data'=>[]]);
		}
		else
		{
			$all_row_status_change = MunicipalityImage::where('municipality_id',$municipality_id)->update(array('is_primary' => 0));
			$municipality_image = MunicipalityImage::find($image_id);
			$municipality_image->is_primary = 1;
			$municipality_image->save();
				
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}
}
