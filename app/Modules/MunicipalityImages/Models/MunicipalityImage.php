<?php

namespace App\Modules\MunicipalityImages\Models;

use Illuminate\Database\Eloquent\Model;

class MunicipalityImage extends Model
{
     public function municipality()
    {
        return $this->belongsTo('App\Modules\\Municipalities\Models\Municipality');
    }
}
