<?php

namespace App\Modules\ForgotPasswords\Controllers;
use App\Modules\Common\Common;
use App\Modules\ForgotPasswords\Models\ForgotPassword;
use App\Modules\Admins\Models\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;
class ForgotPasswordController extends Controller
{
	// get all contact request with pagination , serach
    public function sendEmailForgotPassword(Request $request)
    {
		$all=$request->all();
		
		if(empty($all['email']))
		{
			$email="";
		}
		else{
			$email=$all['email'];
		}
		
		if(empty($email))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter email','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire une-mail con i parametri','data'=>[]]);
		}
		
		$admin=Admin::where('email',$email)->get();
        if(count($admin)>0){
            $code=rand(1,100).''.time();
            $admin_id = $admin[0]->id;
			$forgotpassword = new ForgotPassword;
			$forgotpassword->admin_id   = $admin_id;
			$forgotpassword->code = $code;
			$forgotpassword->save();
			$link=env('APP_URL').'/glocal-admin/#/pages/siteauth/reset-password/'.$code;
			
			try{
					Mail::raw('Hi Admin, \n Please click on the below link to reset your password\n'.$link."\nThanks & Regards,\nGlocal Team", function($message) use ($email) {
					$message->to($email, 'Glocal Team')->subject
					('Glocal admin password reset');
					$message->from('xcitmailer@gmail.com','Glocal');
					});
			}
			catch(\Exception $e){
					// Get error here
				return response()->json(['status'=>0,'message'=>$e->getMessage(),'link'=>$link,'data'=>[]]);
			}
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
        }
        else {
			//return response()->json(['status'=>0,'message'=>'Email does not exist ','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Lemail non esiste','data'=>[]]);
        };
	}

	//reset password
	public function resetPassword(Request $request)
    {
        $all=$request->all();
        if(empty($all['code']))
		{
			$code='';
		}
		else
		{
			$code=$all['code'];
        }
        if(empty($all['new_password']))
		{
			$new_password='';
		}
		else
		{
			$new_password=$all['new_password'];
        }
        if(empty($all['confirm_password']))
		{
			$confirm_password='';
		}
		else
		{
			$confirm_password=$all['confirm_password'];
        }
        if(empty($code))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter code','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Fornire il codice del parametro','data'=>[]]);
        }
        if(empty($new_password))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter new_password','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro new_password','data'=>[]]);
        }
        if(empty($confirm_password))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter confirm_password','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro confirm_password','data'=>[]]);
        }
        if($new_password!=$confirm_password)
        {
			//return response()->json(['status'=>0,'message'=>'New password and confirm password do not match','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'La nuova password e la conferma della password non corrispondono','data'=>[]]);
        }
        else
        {
			$admin=ForgotPassword::where('code',$code)->get();
        	if(count($admin)>0){
				$admin_id = $admin[0]->admin_id;
				$admininfo=Admin::find($admin_id);
				$admininfo->password = md5($new_password);
				$admininfo->save();
				ForgotPassword::where('code',$code)->delete();
				return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
			}
			else{
				//return response()->json(['status'=>0,'message'=>'Code does not exist','data'=>[]]);
				return response()->json(['status'=>0,'message'=>'Il codice non esiste','data'=>[]]);
			}
        }
    }
}
