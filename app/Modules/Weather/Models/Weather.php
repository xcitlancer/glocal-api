<?php

namespace App\Modules\Weather\Models;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model {

    public function municipality() {
        return $this->belongsTo('App\Modules\Municipalities\Models\Municipality');
    }
}
