<?php

namespace App\Modules\TabTypes\Controllers;

use App\Modules\TabTypes\Models\TabType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TabTypeController extends Controller
{
	//get all tab types
	public function getAllTabTypes()
    {
		$tabtype_count = TabType::select('id','type')->orderBy('type','asc')->count();
		if($tabtype_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else{
			$tabtype = TabType::select('id','type')->orderBy('type','asc')->get();
			return response()->json(['status'=>1,'message'=>'success','data'=>$tabtype]);
		}
	}
}
