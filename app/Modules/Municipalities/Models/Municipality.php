<?php

namespace App\Modules\Municipalities\Models;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model {

    public function region() {
        return $this->belongsTo('App\Modules\Regions\Models\Region');
    }

    public function province() {
        return $this->belongsTo('App\Modules\Provinces\Models\Province');
    }

    public function zone() {
        return $this->belongsTo('App\Modules\Zones\Models\Zone');
    }

    public function city() {
        return $this->hasOne('App\Modules\Cities\Models\City', 'name', 'name');
    }

    function municipalityPropertyValues() {
        return $this->hasMany('App\Modules\MunicipalityPropertyValues\Models\MunicipalityPropertyValue');
    }
	public function weather() {
        return $this->hasOne('App\Modules\Weather\Models\Weather');
    }
	public function airquality() {
        return $this->hasOne('App\Modules\Airqualities\Models\Airquality');
    }
    function municipalityImages() {
        return $this->hasMany('App\Modules\MunicipalityImages\Models\MunicipalityImage');
    }
}
