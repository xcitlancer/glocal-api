<?php

namespace App\Modules\Municipalities\Controllers;
use App\Modules\Common\Common;
use App\Modules\Provinces\Models\Province;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Regions\Models\Region;
use Image;
use Illuminate\Support\Facades\Validator;
use App\Modules\Municipalities\Models\Municipality;
use App\Modules\Weather\Models\Weather;
use App\Modules\Airqualities\Models\Airquality;
use App\Modules\MunicipalityTabs\Models\MunicipalityTab;
use App\Modules\MunicipalityTabAttributes\Models\MunicipalityTabAttribute;
use App\Modules\MunicipalityImages\Models\MunicipalityImage;
use Illuminate\Support\Facades\DB;

class MunicipalityController extends Controller
{
	// get municipality list by region id , province id with pagination , serach
    public function getMunicipalityList(Request $request)
    {
		$all=$request->all();
		
		if(empty($all['province_id']))
		{
			$province_id=0;
		}
		else
		{
			$province_id=$all['province_id'];
		}
		
		if(empty($all['region_id']))
		{
			$region_id=0;
		}
		else
		{
			$region_id=$all['region_id'];
		}
		
		if(empty($all['page_no']) && empty($all['_page']))
		{
			$page_no=0;
		}
		else
		{
			$page_no=empty($all['page_no'])?$all['_page']:$all['page_no'];
		}
		if(empty($all['no_of_rows'])&& empty($all['_limit']))
		{
			$no_of_rows=0;
		}else{
			$no_of_rows=empty($all['no_of_rows'])?$all['_limit']:$all['no_of_rows'];
		}
		
		if(empty($all['order_type']) && empty($all['_order']))
		{
			$order_type="asc";
		}
		else{
			$order_type=empty($all['order_type'])?$all['_order']:$all['order_type'];
		}
		$offset = ($page_no - 1) * $no_of_rows;
		
		if(empty($page_no))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter page_no','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro page_no','data'=>[]]);
		}
		if(empty($no_of_rows))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide parameter no_of_rows','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro no_of_rows','data'=>[]]);
		}
		
		$municipalities = Municipality::
				with(['region','province','weather','airquality','municipalityImages']);
		$totalrows=Municipality::select('id','name');
		
		//making dynamic query	
		if(!empty($region_id))
		{
			$municipalities =$municipalities->where('region_id',$region_id);
			$totalrows=$totalrows->where('region_id',$region_id);
		}			
		if(!empty($province_id))
		{
			$municipalities =$municipalities->where('province_id',$province_id);
			$totalrows=$totalrows->where('province_id',$province_id);
		}
		if(!empty($all['name_like']))
		{
			$search_by_name=$all['name_like'];
			$municipalities =$municipalities->where('name', 'LIKE', "$search_by_name%");
			$totalrows=$totalrows->where('name', 'LIKE', "$search_by_name%");
		}
		if(!empty($all['region_like']))
		{
			$region_name=$all['region_like'];
			$municipalities=$municipalities->whereHas('region',function($q) use ($region_name) {
					$q->where('name', 'LIKE', "$region_name%");
				});
			$totalrows=$totalrows->whereHas('region',function($q) use ($region_name) {
					$q->where('name', 'LIKE', "$region_name%");
				});	
		}
		if(!empty($all['province_like']))
		{
			$province_name=$all['province_like'];
			$municipalities=$municipalities->whereHas('province',function($q) use ($province_name) {
					$q->where('name', 'LIKE', "$province_name%");
				});
			$totalrows=$totalrows->whereHas('province',function($q) use ($province_name) {
					$q->where('name', 'LIKE', "$province_name%");
				});	
		}
		$municipalities=$municipalities->orderBy('name',$order_type)->skip($offset)->take($no_of_rows)->get();
		$totalrows=$totalrows->count();

        return response()->json(['status'=>1,'message'=>'success','data'=>$municipalities,'thumbnail_folder_path'=>'municipalityimages/thumbnail','medium_folder_path'=>'municipalityimages/medium','original_folder_path'=>'municipalityimages/original','totalrows'=>$totalrows]);
    }
	// municipality edit description and image
	public function municipalityEditByID(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['id']))
		{
			$id=0;
			
		}
		else
		{
			$id=$all['id'];
		}
		if(empty($all['description']))
		{
			$description='';
			
		}
		else
		{
			$description=$all['description'];
		}
		if(empty($all['image_name']))
		{
			$image_name='';
		}
		else
		{
			$image_name=$all['image_name'];
		}
		if(empty($id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input id','data'=>[]]);
		}
		else if(empty($image_name) && empty($description))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter image_name,description. Nothing to update.','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input image_name,description. Niente da aggiornare.','data'=>[]]);
		}
		else
		{
			if(!empty($image_name))
			{
					$rules = array(
					'image' => 'mimes:jpeg,jpg,png|required|max:100000' // max 100000kb
					);
					$postData = $request->only('image_name');
					$file = $postData['image_name'];
					$fileArray = array('image' => $file);

					$validator = Validator::make($fileArray, $rules);


					if ($validator->fails()) 
					{
						//return response()->json(['status'=>0,'message'=>'Please upload a valid image with max size 100 mb.','data'=>[]]);
						return response()->json(['status'=>0,'message'=>'Carica unimmagine valida con una dimensione massima di 100 MB.','data'=>[]]);
					}
					
					$image = $request->file('image_name');	
					$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
					#image resize
					#thumbnail
					Image::make($request->file('image_name')->getRealPath())->resize(200, 200)->save( public_path('municipalityimages/thumbnail/' . $input['imagename']) );
					
					#medium
					Image::make($request->file('image_name')->getRealPath())->resize(300, 300)->save( public_path('municipalityimages/medium/' . $input['imagename']) );
			
					#original image
					$request->image_name->move(public_path('municipalityimages/original'), $input['imagename']);
				
					$municipality = Municipality::find($id);
					$municipality->municipality_img = $input['imagename'];
					$municipality->save();
					
					
			}
			
					$municipality = Municipality::find($id);
					$municipality->description_txt = $description;
					$municipality->save();
					
			
			if(empty($input['imagename']))
			{
				$imagename_output=[];
			}
			else
			{
				$imagename_output=[ "imagename" => $input['imagename'] ];
			}
			return response()->json(['status'=>1,'message'=>'success','data'=>$imagename_output,'thumbnail_folder_path'=>'municipalityimages/thumbnail','medium_folder_path'=>'municipalityimages/medium','original_folder_path'=>'municipalityimages/original']);
		}
	}
	
	//get municipality details by municipality id
	public function getMunicipalityDetails($municipality_id)
    {
		$municipality_count = Municipality::select('id')->where('id', $municipality_id)->count();
		if($municipality_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else
		{
			$municipality = Municipality::with(['region','province','weather','airquality'])->where('id', $municipality_id)->get();
			return response()->json(['status'=>1,'message'=>'success','data'=>$municipality,'thumbnail_folder_path'=>'municipalityimages/thumbnail','medium_folder_path'=>'municipalityimages/medium','original_folder_path'=>'municipalityimages/original']);
		}
	}
	//municipality tab attribute clone
	public function doMunicipalityClone(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['from_municipality_id']))
		{
			$from_municipality_id=0;
			
		}
		else
		{
			$from_municipality_id=$all['from_municipality_id'];
		}
		
		if(empty($all['to_municipality_id']))
		{
			$to_municipality_id=0;
			
		}
		else
		{
			$to_municipality_id=$all['to_municipality_id'];
		}
		
		if(empty($from_municipality_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter from_municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input from_municipality_id','data'=>[]]);
		}
		if(empty($to_municipality_id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter to_municipality_id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input to_municipality_id','data'=>[]]);
		}
		
		$municipality_tab_count=MunicipalityTab::select('id')->where('municipality_id',$from_municipality_id)->count();
		
		if($municipality_tab_count==0)
		{
			//return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Nessun record trovato','data'=>[]]);
		}
		else
		{
			$municipality_tab = MunicipalityTab::where('municipality_id',$from_municipality_id)->get();
			
			foreach($municipality_tab as $tabvalue)
			{
					$tabid=$tabvalue['id'];
					$municipality_tab = new MunicipalityTab;
					$municipality_tab->name = $tabvalue['name'];
					$municipality_tab->municipality_id  = $to_municipality_id;
					$municipality_tab->tab_type_id  = $tabvalue['tab_type_id'];
					$municipality_tab->status  = 1;
					$municipality_tab->save();
					$new_tab_id=$municipality_tab->id;
					$municipality_tab_attribute_count=MunicipalityTabAttribute::select('id')->where('municipality_tab_id',$tabid)->count();
					if($municipality_tab_attribute_count>0)
					{
							$municipality_tab_attribute=MunicipalityTabAttribute::where('municipality_tab_id',$tabid)->get();
							foreach($municipality_tab_attribute as $tabattrval)
							{
									$municipalitytabattribute = new MunicipalityTabAttribute;
									$municipalitytabattribute->municipality_tab_id  = $new_tab_id;
									$municipalitytabattribute->title = $tabattrval['title'];
									$municipalitytabattribute->content  = $tabattrval['content'];
									$municipalitytabattribute->position  = $tabattrval['position'];
									$municipalitytabattribute->percentage  = $tabattrval['percentage'];
									$municipalitytabattribute->save();
							}
					}
			}
			
			return response()->json(['status'=>1,'message'=>'success','data'=>[]]);
		}
	}
	//municipality add images for image gallery
	
	public function municipalityAddImages(Request $request)
    {
		//validate token
		$token = $request->header('token');
		$validatetoken =Common::validateAdminToken($token);
		if($validatetoken==0)
		{
			//return response()->json(['status'=>2,'message'=>'Invalid token','data'=>[]]);
			return response()->json(['status'=>2,'message'=>'Gettone non valido','data'=>[]]);
		}
		
		$all=$request->all();
		
		if(empty($all['id']))
		{
			$id=0;
			
		}
		else
		{
			$id=$all['id'];
		}
		
		if(empty($all['image_name']))
		{
			$image_name='';
		}
		else
		{
			$image_name=$all['image_name'];
		}
		if(empty($id))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter id','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input id','data'=>[]]);
		}
		else if(empty($image_name))
		{
			//return response()->json(['status'=>0,'message'=>'Please provide input parameter image_name. Nothing to add.','data'=>[]]);
			return response()->json(['status'=>0,'message'=>'Si prega di fornire un parametro di input image_name. Niente da aggiungere.','data'=>[]]);
		}
		else
		{
					$rules = array(
					'image' => 'mimes:jpeg,jpg,png|required|max:100000' // max 100000kb
					);
					$postData = $request->only('image_name');
					$file = $postData['image_name'];
					$fileArray = array('image' => $file);

					$validator = Validator::make($fileArray, $rules);


					if ($validator->fails()) 
					{
						//return response()->json(['status'=>0,'message'=>'Please upload a valid image with max size 100 mb.','data'=>[]]);
						return response()->json(['status'=>0,'message'=>'Carica unimmagine valida con una dimensione massima di 100 MB.','data'=>[]]);
					}
					
					$image = $request->file('image_name');	
					$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
					#image resize
					#thumbnail
					Image::make($request->file('image_name')->getRealPath())->resize(200, 200)->save( public_path('municipalityimages/thumbnail/' . $input['imagename']) );
					
					#medium
					Image::make($request->file('image_name')->getRealPath())->resize(300, 300)->save( public_path('municipalityimages/medium/' . $input['imagename']) );
			
					#original image
					$request->image_name->move(public_path('municipalityimages/original'), $input['imagename']);
					
					$image_count=MunicipalityImage::where('municipality_id',$id)->where('is_primary',1)->count();
					if($image_count==0){
						$is_primary=1; //set to primary
					}else{
						$is_primary=0;
					}
					$municipality = new MunicipalityImage;
					$municipality->img = $input['imagename'];
					$municipality->municipality_id  = $id;
					$municipality->is_primary  = $is_primary;
					$municipality->save();
					
					$imagename_output=[ "imagename" => $input['imagename'] ];
			
					return response()->json(['status'=>1,'message'=>'success','data'=>$imagename_output,'thumbnail_folder_path'=>'municipalityimages/thumbnail','medium_folder_path'=>'municipalityimages/medium','original_folder_path'=>'municipalityimages/original']);
		}
	}
	
	//get municipality name

	public function searchMunicipalityName(Request $request)
    {
		$all=$request->all();
		$order_type="asc";
		$municipalities = Municipality::select('name');
		$output_merge_array=[];
		if(!empty($all['query']))
		{
			$search_by_name=$all['query'];
			$region_array=[];
			$province_array=[];
			$municipalitie_array=[];
			//get region info
			$regions = DB::table('regions')
			->select('name','region_img')
			#->leftJoin('posts', 'users.id', '=', 'posts.user_id')
			->where('name', 'LIKE', "$search_by_name%")
			->get();
			
			

			foreach ($regions as $region) {
				if($region->region_img!=""){
					$thumbnail_image=env('APP_URL').'/glocal-api/public/regionimages/thumbnail/'.$region->region_img;
				}else{
					$thumbnail_image="";
				}
				$region_array[]=array(
					'name' =>$region->name,
					'type' =>"Region",
					"thumbnail_image"=>$thumbnail_image
				);
			}
			//get province info
			$provinces = DB::table('provinces')
			->select('name','province_img')
			->where('name', 'LIKE', "$search_by_name%")
			->get();
			
			foreach ($provinces as $province) {
				if($province->province_img!=""){
					$thumbnail_image=env('APP_URL').'/glocal-api/public/provinceimages/thumbnail/'.$province->province_img;
				}else{
					$thumbnail_image="";
				}
				$province_array[]=array(
					
					'name' =>$province->name,
					'type' =>"Province",
					"thumbnail_image"=>$thumbnail_image

				);
			}

			// get municipality info
			$municipalities = DB::table('municipalities')
			->select('name','img')
			->leftJoin(DB::raw("(select img,is_primary,municipality_id from municipality_images where is_primary=1 ) as dbimg"), 'dbimg.municipality_id', '=', 'municipalities.id')
			->where('name', 'LIKE', "$search_by_name%")
			->skip(0)
			->take(10)
			->get();
			
			foreach ($municipalities as $municipality) {
				if($municipality->img!=""){
					$thumbnail_image=env('APP_URL').'/glocal-api/public/municipalityimages/thumbnail/'.$municipality->img;
				}else{
					$thumbnail_image="";
				}
				$municipalitie_array[]=array(
					
					'name' =>$municipality->name,
					'type' =>"Municipality",
					"thumbnail_image"=>$thumbnail_image

				);
			}
			//merge all arrays into one array
			$output_merge_array=array_merge($region_array,$province_array,$municipalitie_array);
			array_multisort(array_column($output_merge_array, 'name'), SORT_ASC, $output_merge_array);
			//echo "<pre>";
			//print_r($output_merge_array);
			//echo "</pre>";

			/*
			$totalrows=Municipality::select('name')->where('name', 'LIKE', "$search_by_name%")->count();
			if($totalrows>0)
			{
				$municipalities =$municipalities->where('name', 'LIKE', "$search_by_name%");
			}
			else
			{
				return response()->json(['status'=>0,'message'=>'No record found','data'=>[]]);
			}*/
		}
		#$municipalities=$municipalities->orderBy('name',$order_type)->skip(0)->take(10)->get();
		return response()->json(['status'=>1,'message'=>'success','data'=>$output_merge_array]);
    }
}