<?php


use Illuminate\Support\Facades\Route;

use App\Modules\Admins\Controllers\AdminController;
use App\Modules\Provinces\Controllers\ProvinceController;
use App\Modules\Crons\Controllers\CommuniController;
use App\Modules\Regions\Controllers\RegionController;
use App\Modules\Municipalities\Controllers\MunicipalityController;
use App\Modules\TabTypes\Controllers\TabTypeController;
use App\Modules\MunicipalityTabs\Controllers\MunicipalityTabController;
use App\Modules\MunicipalityTabAttributes\Controllers\MunicipalityTabAttributeController;
use App\Modules\MunicipalityImages\Controllers\MunicipalityImageController;
use App\Modules\Reviews\Controllers\ReviewController;
use App\Modules\ContactRequests\Controllers\ContactRequestController;
use App\Modules\TopMunicipalities\Controllers\TopMunicipalityController;
use App\Modules\ForgotPasswords\Controllers\ForgotPasswordController;
/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

##test route
Route::get('/', function () {
    return response()->json(["status"=>1,"message"=>"API Working"]);
});

## authentications
Route::post('login', [AdminController::class, 'login']);

## regions
Route::get('regions', [RegionController::class, 'getAllRegions']);
Route::get('regionslist', [RegionController::class, 'getAllRegionsList']);
Route::post('regionedit', [RegionController::class, 'regionEditByID']);
Route::get('regiondetails/{id}', [RegionController::class, 'getRegionDetails']);

## provinces
Route::get('provinces', [ProvinceController::class, 'getProvincesByRegion']);
Route::get('provincedetails/{id}', [ProvinceController::class, 'getProvincesDetails']);
Route::post('provinceedit', [ProvinceController::class, 'provinceEditByID']);

## Municipality
Route::get('municipalities', [MunicipalityController::class, 'getMunicipalityList']);
Route::post('municipalityedit', [MunicipalityController::class, 'municipalityEditByID']);
Route::get('municipalitydetails/{id}', [MunicipalityController::class, 'getMunicipalityDetails']);
Route::post('municipalityclone', [MunicipalityController::class, 'doMunicipalityClone']);

##Search Municipality
Route::get('searchmunicipalityname', [MunicipalityController::class, 'searchMunicipalityName']); 

## Municipality gallery images 
Route::post('municipalityaddimages', [MunicipalityController::class, 'municipalityAddImages']);
Route::get('municipalityallimages', [MunicipalityImageController::class, 'getAllMunicipalityImages']);
Route::post('municipalitydeleteimages', [MunicipalityImageController::class, 'municipalityImageDelete']);
Route::post('municipalityimagesetprimary', [MunicipalityImageController::class, 'municipalityImageSetPrimary']);

#Municipality reviews
Route::get('reviews', [ReviewController::class, 'getAllReviews']);
Route::post('reviewstatuschange', [ReviewController::class, 'reviewStatusChange']);
Route::post('reviewadd', [ReviewController::class, 'reviewAdd']);

## tab types
Route::get('tabtypes', [TabTypeController::class, 'getAllTabTypes']);

## municipality tabs
Route::get('municipalitytabs', [MunicipalityTabController::class, 'getAllMunicipalityTabs']);
Route::post('municipalitytabadd', [MunicipalityTabController::class, 'municipalityTabAdd']);
Route::get('municipalitytabdetails/{id}', [MunicipalityTabController::class, 'getMunicipalityTabDetails']);
Route::post('municipalitytabedit', [MunicipalityTabController::class, 'municipalityTabEdit']);
Route::post('municipalitytabdelete', [MunicipalityTabController::class, 'municipalityTabDelete']);
Route::get('municipalityalltabs', [MunicipalityTabController::class, 'getMunicipalityTabs']);

##tab attributes
Route::post('municipalitytabattributesadd', [MunicipalityTabAttributeController::class, 'municipalityTabAttributeAdd']);
Route::get('municipalitytabattributes/{id}', [MunicipalityTabAttributeController::class, 'getMunicipalityTabAttribute']);
Route::get('municipalitytabattributesinfo/{id}', [MunicipalityTabAttributeController::class, 'getMunicipalityTabAttributeInfo']);
##update order
Route::post('tabattributeupdateorder', [MunicipalityTabAttributeController::class, 'tabAttributeUpdateOrder']);

##contact requests
Route::get('contactrequests', [ContactRequestController::class, 'getAllContactRequests']);
Route::post('contactrequestsadd', [ContactRequestController::class, 'contactRequestsAdd']);

##change password
Route::post('changepassword', [AdminController::class, 'changePassword']);

##change email
Route::post('changeemail', [AdminController::class, 'changeEmail']);

##get user details
Route::post('getuserdetails', [AdminController::class, 'getUserDetails']);

##top municipalities
Route::get('topmunicipalities', [TopMunicipalityController::class, 'getTopMunicipalities']);
Route::post('topmunicipalityadd', [TopMunicipalityController::class, 'topMunicipalityAdd']);
Route::post('topmunicipalitydelete', [TopMunicipalityController::class, 'topMunicipalityDelete']);
Route::get('gettopmunicipalities', [TopMunicipalityController::class, 'getAllTopMunicipalities']);

##forgot passsword
Route::post('sendemailforgotpassword', [ForgotPasswordController::class, 'sendEmailForgotPassword']);
##reset password
Route::post('resetpassword', [ForgotPasswordController::class, 'resetPassword']);

## settings
#Route::post('getSettings', 'SettingsController@getSettings');


